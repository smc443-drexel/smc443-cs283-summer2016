#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* track_sportsfans(void* counter_p);

int main()
{
	int num_sportsfans = 100;

	int counter = 0;
	pthread_t sportsfans[num_sportsfans];

	int i;

	for (i = 0; i < num_sportsfans; i++)
	{
		pthread_create(&sportsfans[i], NULL, track_sportsfans, (void*)(&counter));
		//{
		//	fprintf(stderr, "Error in sportsfan creation. Aborting...\n");
		//	exit(1);
		//}
	}

	for (i = 0; i < num_sportsfans; i++)
	{
		pthread_join(sportsfans[i], NULL);
	}

	printf("Sportsfan movements counted: %d.\n", counter);
}

void* track_sportsfans(void* counter_p)
{
	int* counter;
	int sportsfan_moves, i;

	counter = (int*)counter_p;
	sportsfan_moves = 10000;

	for(i = 0; i < sportsfan_moves; i++)
		*counter = *counter + 1;

	//printf("woo%d", *counter);
}
