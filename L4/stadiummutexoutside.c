#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* track_sportsfans(void* counter_p);

pthread_mutex_t my_lock;

int main()
{
	int num_sportsfans = 100;
	
	if (pthread_mutex_init(&my_lock, NULL) != 0)
	{
		printf("Error in lock creation. Aborting...");
		exit(1);
	}

	int counter = 0;
	pthread_t sportsfans[num_sportsfans];

	int i;

	for (i = 0; i < num_sportsfans; i++)
	{
		if (pthread_create(&sportsfans[i], NULL, track_sportsfans, (void*)(&counter)) != 0)
		{
			fprintf(stderr, "Error in sportsfan creation. Aborting...\n");
			exit(1);
		}
	}

	for (i = 0; i < num_sportsfans; i++)
	{
		pthread_join(sportsfans[i], NULL);
	}

	printf("Sportsfan movements counted: %d.\n", counter);
}

void* track_sportsfans(void* counter_p)
{
	int* counter;
	int sportsfan_moves, i;

	counter = (int*)counter_p;
	sportsfan_moves = 10000;

	pthread_mutex_lock(&my_lock);

	for(i = 0; i < sportsfan_moves; i++)
	{
		//pthread_mutex_lock(&my_lock);
		*counter = *counter + 1;
		//pthread_mutex_unlock(&my_lock);
	}

	pthread_mutex_unlock(&my_lock);

	//printf("woo%d", *counter);
}
