#!/usr/bin/env python

import sys

# Input via standard input, with the current expectation being
# that it will be piped from a file.
for line in sys.stdin:
    line = line.strip()
    words = line.split(',')

    # Check the train line to make sure it's a number.
    try:
        x = int(words[0])
    except ValueError:
        continue
    
    # Simplify the train line number, as per the note on Piazza.
    x = (x % 1000) / 100

    # We write the result of the line to standard output, to be
    # carted off to the reducer.
    #
    # Output is of the format:
    # train line    on-time counter (0/1)   total counter (1)
    if words[5] == "On Time" or words[5] == "1 min" or words[5] == "2 min" or words[5] == "3 min" or words[5] == "4 min" or words[5] == "5 min":
        print '%s\t%s\t%s' % (x, 1, 1)   # On time.
    else:
        print '%s\t%s\t%s' % (x, 0, 1)   # Late.

