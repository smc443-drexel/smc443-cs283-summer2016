#!/usr/bin/env python

from operator import itemgetter
import sys

current_tline = None
current_ontime = 0
current_count = 0
tline= None

print '%-5s  %-15s  %-15s  %-5s' % ("Line:", "On Time:", "Total:", "Average:")

for line in sys.stdin:
    line = line.strip()
    tline, ontime, count = line.split('\t', 2)

    # In case we've been passed a faulty ontime value.
    try:
        ontime = int(ontime)
    except ValueError:
        continue

    # In case we've been passed a faulty incrementer value.
    try:
        count = int(count)
    except ValueError:
        continue

    # The expectation here is that the reducer will be passed a
    # key-sorted sequence of data.
    if current_tline == tline:
        current_ontime += ontime
        current_count += count
    else:
        if current_tline: # We've hit the end for this train line.
            print '%-5s  %-15s  %-15s  %-3.2f%%' % (current_tline, current_ontime, current_count, ((float(current_ontime) / float(current_count)) * 100))
        current_ontime = ontime
        current_count = count
        current_tline = tline

# Handle the output of the final train line.
if current_tline == tline:
    print '%-5s  %-15s  %-15s  %-3.2f%%' % (current_tline, current_ontime, current_count, ((float(current_ontime) / float(current_count)) * 100))
